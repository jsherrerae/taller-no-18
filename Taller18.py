import matplotlib.pyplot as plt

x = [0,1,2,3,4,5,6,7,8,9,10]
y = []
z = []
w = []
for i in range( len(x) ):
	y.append( x[i]**2 )
	z.append( x[i]**2  -  10 )
	w.append( x[i]**2  - 20 )

plt.plot(x, y, "r")
plt.plot(x, z, "b")
plt.plot(x, w, "k")
plt.ylabel( "Eje X" )
plt.xlabel( "Eje Y" )
plt.show()